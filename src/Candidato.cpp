#include "Candidato.hpp"
#include <iostream>

Candidato::Candidato(){
	NR_Candidato = 0;
	Nome_urna_candidato = "";
	Nome_Partido = "";
}

Candidato::~Candidato(){
	//cout << "Candidato destruído" << endl;
}

int Candidato::get_NR_Candidato(){
	return NR_Candidato;
}
void Candidato::set_NR_Candidato(int NR_Candidato){
	this -> NR_Candidato = NR_Candidato;
}
string Candidato::get_Nome_urna_candidato(){
	return Nome_urna_candidato;
}
void Candidato::set_Nome_urna_candidato(string Nome_urna_candidato){
	this -> Nome_urna_candidato = Nome_urna_candidato;
}
string Candidato::get_Nome_Partido(){
	return Nome_Partido;
}
void Candidato::set_Nome_Partido(string Nome_Partido){
	this -> Nome_Partido = Nome_Partido;
}
bool Candidato::apto_inapto(int sinal){
	if (sinal == 1) return true;
	else return false;
}





