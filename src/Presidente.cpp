#include <iostream>
#include "Presidente.hpp"

using namespace std;

Presidente::Presidente(){
	//cout << "Construindo Presidente" << endl;
	set_NR_Candidato(0);
	set_Nome_urna_candidato("");
	set_Nome_Partido("");
	set_Vice_Presidente("");
	set_Partido_Vice("");

}
Presidente::~Presidente(){
	//cout << "Destruindo Presidente" << endl;
}
string Presidente::get_Vice_Presidente(){
	return Vice_Presidente;
}
void Presidente::set_Vice_Presidente(string Vice_Presidente){
	this -> Vice_Presidente = Vice_Presidente;
}
string Presidente::get_Partido_Vice(){
	return Partido_Vice;
}
void Presidente::set_Partido_Vice(string Partido_Vice){
	this -> Partido_Vice = Partido_Vice;
}
