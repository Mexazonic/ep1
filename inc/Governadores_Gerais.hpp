#ifndef GOVERNADORES_GERAIS_HPP
#define GOVERNADORES_GERAIS_HPP
#include <bits/stdc++.h>
using namespace std;
#include "Candidato.hpp"

class Governadores_Gerais : public Candidato {

private:
	string Unidade_Estadual;
	string Suplentes;
	string Partido_Suplentes;
	string Suplentes2;
	string Partido_Suplentes2;
public:
	Governadores_Gerais();
	~Governadores_Gerais();

	string get_Unidade_Estadual();
	void set_Unidade_Estadual(string Unidade_Estadual);
	string get_Suplentes();
	void set_Suplentes(string Suplentes);
	string get_Suplentes2();
	void set_Suplentes2(string Suplentes2);
	  string get_Partido_Suplentes();
	void set_Partido_Suplentes(string Partido_Suplentes);
	string get_Partido_Suplentes2();
	void set_Partido_Suplentes2(string Partido_Suplentes2);
};

#endif
