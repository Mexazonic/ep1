//Bruno Henrique Sousa Duarte
//17_0138551
#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include <bits/stdc++.h>
using namespace std;

class Candidato {


//Atributos
	private:
		int NR_Candidato;
		string Nome_urna_candidato;
		string Nome_Partido;
	
	//Métodos
	public:
	   	Candidato();
		~Candidato();
		
	       int get_NR_Candidato();
       		void set_NR_Candidato(int NR_Candidato);
		string get_Nome_urna_candidato();
		void set_Nome_urna_candidato(string Nome_urna_candidato);
		string get_Nome_Partido();
		void set_Nome_Partido(string Nome_Partido);		
		bool apto_inapto(int sinal);

};

#endif
