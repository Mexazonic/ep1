#ifndef PRESIDENTE_HPP
#define PRESIDENTE_HPP
#include <bits/stdc++.h>
using namespace std;
#include "Candidato.hpp"

class Presidente : public Candidato {

private:
	string Vice_Presidente;
	string Partido_Vice;
public:
	Presidente();
	~Presidente();
	
	string get_Vice_Presidente();
	void set_Vice_Presidente(string Vice_Presidente);
	string get_Partido_Vice();
	void set_Partido_Vice(string Partido_Vice);
};
#endif
