# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

* Compile o projeto com o comando:

```sh
make
```

* Execute o projeto com o comando:

```sh
make run
```

## Funcionalidades do projeto

O projeto consegue ler e retornar os dados do voto de até 166 eleitores devido a matriz memset[1000][1000];
Utilizei a estrutura map para não usar algoritmos de força bruta sendo que este ep1 mostrou-se como uma boa oportunidade de testar a biblioteca do C++;
Para votar em Branco basta digitar "Branco" ou "branco" e nulo caso o numero não exista;
O projeto teve um número considerado de uso da estrutura "for"; 
Utilizei branch para testar as ideias de códigos e os conceitos de ramos também, utilizei mais os arquivos da branch do que os da master caso notem um período de desuso grande desses arquivos enviados aqui;

## Bugs e problemas
Como existe candidatos inaptos e aptos, não consegui pensar em como identificar os substitutos então acabei desconsiderando e não utilizando esta função;
Não consegui tirar o Eleitor no final do relatório, achei que o relatório era como um voto impresso para o usuário confirmar, depois percebi que era um relatório geral e acabei pensando de última hora;
Não consegui retirar os warnings;
Por tratar de strings problemas com buffer de memória eram constantes;
## Referências

Uma das minhas referências foi o material disponibilizado pelo professor Matheus Faria em seu GitHub e suas aulas.

https://github.com/MatheusFaria/TEP/blob/master/Introducao/STL.md
